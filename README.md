# Inkscape GSoC19 application draft

## Preamble

Hey, everyone, I am in need for advice from the developers around. As I have
stated several times already, I am looking to apply to Inkscape to GSoC and in
order to do that, I have already had a few PRs merged, on translation,
packaging and extensions.

## About myself

My name is Valentin Ioniță and I am a sophomore student in Computer Science and
Engineering at University Politehnica of Bucharest, Romania, south-eastern
Europe. I am experienced in front-end web development and graphic design, being
self-taught in both domains. I am accustomed with ES6+, npm packaging and have
taught at an Angular workshop.

Four years ago I started contributing on StackOverflow (now I am shy of 2k
points) and stumbled upon the open-source community. A couple of years ago,
along with starting college, I finally grasped the value of open-source and
its importance to the technology world. Now I am aware of how
many beautiful, useful things, as both an user and a developer, come from
this environment and I am interested in contributing myself, bit by bit, in my
following career. As a proof of concept, last year I have participated to
Hacktoberfest, where I pushed five MRs to repositories on Github.

I am consistently building up my skills and learning (currently learning Vue,
designing UIs and stationary and planning for a portfolio in Angular) in order
to contribute from now on to projects and communities that I find exciting and
useful. Inkscape is one of those projects, as I have repeatedly used the app to
practice design, create assets for student events and help people that needed
graphics.

### Summer plans

For this summer, I am actively planning to:

- be working on GSoC
- going through the Github campus Expert program
- taking courses in Java, Python and C++ from GeeksForGeeks (to test myself in
  the languages I learned in the second college year)
- going through Freecodecamp and Egghead courses to deepen my web knowledge
- going through algorithms interview questions (in order to apply for an
  internship in the third year) from Programming Praxis and Hackerank
- be actively creating projects in each of the above, to prove my skills

### Contact info and more

You can check out my [resume][1] or my [website][2] (under reconstruction).

You can find me on most social platforms and IRC with the handle `@vanntile`

- [LinkedIn][3]

- [Github][4] where you can check two of my web projects [Medicare][5] and
[Javascript30][6] they both have documentations up to date, so you can check
them on Github directly. Furthermore, I have worked at an internship on a
delivery tracing web app, as described in my resume

- [StackOverflow][7]

- [Medium][8] for a few articles on design

- [Facebook][9]

- [Instagram][10] and [Dribbble][11] where you can check my designs

### Tool Stack

I have or I am currently using

- Editors: Sublime, Atom (current), Visual Studio Code, the JetBrains stack
(IntelliJ, PyCharm, CLion), gdb, IDA Dissasembler, Vim
- Grapics: Inkscape, GIMP, Blender, Figma
- Versioning: Github, Gitlab, Bitbucket
- Automation: Gulp, Grunt, npm
- I am an Ubuntu 18.04 user, and my browser of choice is Firefox

### Other GSoC

Other than Inkscape, where I'm driven by passion, I am applying to Amahi, where
I am the right fit for their web needs, being both a designer and developer.

## How I use Inkscape

You can see most of my graphics work on [Instagram][10] and [Dribbble][11] and
should check out how I turned a team into a family using Inkscape (the story and
design process can be found [here][12] or how I simulated [grain textures][13]
in Inkscape.

You can find some of my source files (or the `.png` exports) in the [graphics][14]
folder of the repo. Here is a small preview.

![cover][15]

I am a graphic design volunteer for the student association in my college and I
have worked on various projects in the last couple of years, getting to be a
design proect manager for a student hackathon. You can view my work in this
repo’s [LSAC][16] folder. Working using Inkscape has raised some compatibility
issues with closed software such as Adobe Illustrator, but it all worked out
and, at the beginning of this school year, I have co-held a design workshop and
plan doing another one on typography son. I had the opportunity of teaching and
leading students to design and it’s all thanks to hard work and Inkscape.

## Why me?

I am passionate, involved, I have experience with Inkscape and I was planning
for some time to contribute to this project (even after the end of this program,
I will continue to bring improvements and help the community grow). Moreover, I
can use the view of a designer, developer and student, and in the foreseeable
future maybe even become a mentor myself at this organisation.

## Inkscape MRs

I have already become accustomed with the Inkscape developers community: I am on
the Rocket chat and subscribed to most of the newsletters. I have got to know
some amazing people, learned some dev/prod tips, heard some amazing arguments
on the state of the web and open-source, found great UI designers and watched
live an Inkscape board meeting. I have build Inkscape itself, checked all other
secondary projects and contributed to packaging Inkscape, the extensions group,
a documentation improvement for lib2geom, content for the website (a WIP) and
the Romanian translation. These are all linked below.

### Packaging

[Rename Linux metadata (Fixes #22)][17]

[Appdata file update (Fix #30)][18]

### Extensions

[fix DeprecationWarnings for ungroup_deep.py][19]

[Transform.compute_point for barcode fixes #39][20]

### lib2geom

[Convert docs to Markdown fixes #10][21]

### Content

[Sponsor icons update][22]

### Translations

[Update ro.po][23]

## Project Proposal

As it is specified in SoC application template on the wiki, I want to introduce
my plans for contributing. I want to do mainly two things: improve the CSS
support and add JS polyfills. Why two? Because I am capable and they're both
in my skillsets. I believe these two are essential because they can be expanded
upon. If we get a way of polyfilling Inkscape-specific functionality, I think
it's the start for different mesh-gradients development, animations and other
'imported' functionality from other graphics software. Both are mentored by
Tavmjong Bah.

I know I previously mentioned two projects, but I found both of them suitable to
skill set and my interests. I will do my best to finish them both during GSoC.
I will start working on JS Polyfills and I estimate that I can finish it in one
month. Afterwards, I will move on to CSS support. I believe this is doable,
taking into consideration my availability (most of the summer and even the
university year) and the fact that these two projects have the same mentor.
However, I plan to become a regular contributor and to continue helping the
community even after the end of this program, so I will dedicate the time needed
to finish both and more afterwards.

To put on paper a short roadmap for the two:

### JavaScript Polyfills

- Week 1: List major CSS features implemented in Inkscape
- Asses the status of SVG2 features drafts and real implementations between
browsers and frameworks
- Check the Working Draft from W3C of the corresponding features
- Week 2: Document existing external polyfills
- Document how Inkscape can use polyfills
- Prototype approach for all future implementations
- Week 3: Start implementation
- Implement and document (really important)
- Week 5: Test support of the implementation both in Inkscape and in browsers for the
end-result `svg`

### Cascading Style Sheet Support

- Week 6: Getting knowledgeable with the previous GSoC projects
- Week 7: Cover internal code for style sheet parsing
- Describe use cases
- Week 8: Prototype for actual use and user experience (maybe with team-ux)
- Week 9: Start backend implementation
- Week 11: Expand on the interface
- Implement and document

------

[1]: ./Resume_Valentin_Ionita.pdf
[2]: http:vanntile.tk
[3]: https://www.linkedin.com/in/valentin-ionita/
[4]: https://github.com/vanntile
[5]: https://github.com/vanntile/medicare
[6]: https://vanntile.github.io/JavaScript30/
[7]: https://stackoverflow.com/users/4679160/vanntile-ianito
[8]: https://medium.com/@vanntile
[9]: https://www.facebook.com/vanntile.ianito
[10]: https://www.instagram.com/vanntile/
[11]: https://dribbble.com/vanntile
[12]: https://www.linkedin.com/pulse/what-ifttus-valentin-ionita/
[13]: https://medium.com/vanntile-factory/design-challenge-grain-texture-in-inkscape-f6e185c4c067
[14]: ./graphics
[15]: ./graphics/cover.png
[16]: ./graphics/LSAC
[17]: https://gitlab.com/inkscape/inkscape/merge_requests/501
[18]: https://gitlab.com/inkscape/inkscape/merge_requests/502
[19]: https://gitlab.com/inkscape/extensions/merge_requests/46
[20]: https://gitlab.com/inkscape/extensions/merge_requests/64
[21]: https://gitlab.com/inkscape/lib2geom/merge_requests/11
[22]: https://gitlab.com/inkscape/vectors/content/commit/a4172e2c6bcc7be803cb4b10351a24aef9336189
[23]: https://gitlab.com/inkscape/inkscape/merge_requests/467
